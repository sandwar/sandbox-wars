# README #

Welcome to SandBox Wars repository, this is where the source code of SandBox
Wars is hosted and maintained. Feel free to have a look around and experiment
with the code.

### What is this repository for? ###

This repository contains SandBox Wars and related resources' source codes.

### How do I get set up? ###

A C# development environment is required, I suggest to use
[SharpDevelop](http://www.icsharpcode.net/OpenSource/SD/) on Windows,
[Xamarin Studio](http://xamarin.com/studio) on MacOS, and
[MonoDevelop](http://monodevelop.com/) on Linux.
[Visual Studio](http://www.visualstudio.com/) should also be fine.

UnitTests require [NUnit](http://www.nunit.org/) and an IDE that supports it.

### Contribution guidelines ###

Contributions are not accepted right now, but the source code is free to
experiment with, subject to the (although permissive) restrictions of the
license.

If you really want to contribute with the development of SandBox Wars, please
contact the author.

### Who do I talk to? ###

Feel free to contact me at
[fabiogiopla@gmail.com](mailto:fabiogiopla@gmail.com?subject=About%20SandBox%20Wars)
for any question, suggestion or complaint.

Thanks for showing interest in my project, I hope you will enjoy playing it
as much as I enjoy developing it!

